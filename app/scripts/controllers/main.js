angular.module('tubeziqApp')

.filter('code', ['$sce', function ($sce) {
    return function(url) {
        var code = url.split('&')[0].split('=')[1];
        return code;
    };
}])

.controller('MainCtrl', function ($scope,$http,$filter,utilFactory,services, Reddit) {
        
        $scope.reddit = new Reddit();
        console.log($scope.reddit); 
                    

        $scope.images = [1, 2, 3, 4, 5, 6, 7, 8];

        $scope.loadMore = function() {
          var last = $scope.images[$scope.images.length - 1];
          for(var i = 1; i <= 8; i++) {
            $scope.images.push(last + i);
          }
        };

        var playAudio   = true;
        var params      = { allowScriptAccess: "always" };
        var atts        = { id: "myytplayer" };
        swfobject.embedSWF("http://www.youtube.com/v/bHQqvYy5KYo?enablejsapi=1&playerapiid=ytplayer&version=3",
                           "ytapiplayer", "425", "356", "8", null, null, params, atts);
        
        $scope.term     = 'chillstep';
        $scope.playIcon = 'fa-play';
        $scope.muteIcon = 'fa-volume-up';
        $scope.timer;

        var time         = 0;
        var elapsedTime  = 0;
        var durationTime = 0;
        var duration     = 0;

        // Display current playback time
        $scope.getCurrentTime = function(){
            
            // If digest not in progress, apply
            if(!$scope.$$phase) {
                $scope.$apply(function(){
                    time = utilFactory.secondsToMinutes( ytplayer.getCurrentTime() );
                    elapsedTime = time.minutes.toFixed() + ':' + time.seconds.toFixed();

                    durationTime = utilFactory.secondsToMinutes( ytplayer.getDuration() );          
                    duration  = durationTime.minutes.toFixed() + ':' + durationTime.seconds.toFixed();;          
                    $scope.elapsedTime = elapsedTime + ' / ' + duration;
                })
            }

            // Update playback time every second
            $scope.timer = setTimeout($scope.getCurrentTime, 1000);
        };

        // Load song selected from search results
        $scope.loadSong = function( code ){
            $scope.selected = code;
            code                = $filter('code')(code);
            $scope.showControls = true;
            $scope.playIcon     = 'fa-pause';
            myytplayer.loadVideoById(code, 5, "large")
            playAudio           = false;

            $scope.getCurrentTime();
        };

        // Play and pause
        $scope.play = function(){
            if(playAudio){
                $scope.playIcon = 'fa-pause';
                ytplayer.playVideo();                            
                $scope.getCurrentTime();
                playAudio = false;
            }
            else{
                clearTimeout($scope.timer);
                $scope.playIcon = 'fa-play';
                ytplayer.stopVideo();
                playAudio = true;
            }
        };

        // Mute playback
        $scope.mute = function(){
            if( ytplayer.isMuted() ) {
                $scope.muteIcon = 'fa-volume-up';
                ytplayer.unMute();
            }
            else{
                $scope.muteIcon = 'fa-volume-off';
                ytplayer.mute();
            }  
        };

        $scope.search = function(){
            var options = {searchTerm: $scope.term, maxResults:20 }

            services.search( options )
                .success(function(data, status, headers, config) {
                    $scope.songs = data.feed.entry;
                }).
               error(function(data, status, headers, config) {
               });
            
    };
  
})

// Reddit constructor function to encapsulate HTTP and pagination logic
.factory('Reddit', function($http) {
  var Reddit = function() {
    this.songs = [];
    this.busy = false;
    this.after = '';
  };
    var start = 5;

  Reddit.prototype.nextPage = function() {
    _this = this;
    if (_this.busy) return;
    _this.busy = true;

    setTimeout(function() {
    
        var url = "https://gdata.youtube.com/feeds/api/videos?q=chillstep&duration=long&enablejsapi=1&max-results=5&caption=false&v=2&alt=json&callback=JSON_CALLBACK&start-index=" + start;
          start += 5 
                    
        // var url = "http://api.reddit.com/hot?after=" + this.after + "&jsonp=JSON_CALLBACK";
        $http.jsonp(url).success(function(data) {
                        
            var songs = data.feed.entry;
            for (var i = 0; i < songs.length; i++) {           
                _this.songs.push(songs[i]);
            }
                  
    }.bind(this));
      _this.busy = false;
    }, 1500);
  };



  return Reddit;
});